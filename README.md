# K9-means Clustering
###### Background
This is a [k-means clustering model](https://towardsdatascience.com/k-means-clustering-algorithm-applications-evaluation-methods-and-drawbacks-aa03e644b48a) that handles 12,000 records of body measurements to differentiate between six dog breeds: the Collie, Alaskan Malamut, Irish Wolfhound, Pembroke Welsh Corgi, Dalmatian and Chihuahua.
K-means clustering is a popular type of unsupervised machine learning that is often applied to uncategorized data to find groups (K) within the data that feature similarities. This process is done iteratively by assigning each data point/record to a group based on similarity.
While generally effective and performant in exploratory analyses, the drawbacks of k-means become pronounced when dealing with data replete with outliers and factor complexity.
## Result
![](output.png)
## Usage
###### __Prerequisites
[gnuplot](http://www.gnuplot.info/) is required to graph the datasets.
###### __From given files
To visualize the given clustered data, simply run `cat results.csv | cargo run --bin plot_clusters` in the terminal. If, however, a new set of data is to be generated, continue reading below.
###### __Editing parameters and starter data groups
The parameters and starter data groups can be found and edited in the `config` folder. Each row under `centroids` represents the core of each data group where all of the data records will generate around upon running the `generate` scripts.
###### __Generating datasets
***From the root project location,*** run `cargo run --bin generate -- --config-file config/generate.toml > [insert_file_name_here].csv` in the terminal.
To generate a clustered dataset from the training data run `cat [insert_name_of_training_data_here].csv | cargo run --bin cluster > [insert_different_file_name_here].csv` in the terminal.
###### __Visualizing the data
To plot the training data, run `cat [insert_training_data_name_here].csv | cargo run --bin plot` in the terminal. Again, ***from the root project location.***
To plot the clustered data, run `cat [insert_clustered_data_name_here].csv | cargo run --bin plot_clusters` in the terminal.