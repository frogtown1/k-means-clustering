use std::error::Error;
use gnuplot::{Figure, Caption, Graph};
use gnuplot::AxesCommon;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    let mut x: Vec<f64> = Vec::new();
    let mut y: Vec<f64> = Vec::new();
    let mut reader = csv::Reader::from_reader(io::stdin());

    for result in reader.records() {
        let record = result?;
        x.push(record[0].parse().unwrap());
        y.push(record[1].parse().unwrap());
    }
    
    let mut fg = Figure::new();
    fg.axes2d()
        .set_title("Dog Body Measurements", &[])
        .set_legend(Graph(0.9), Graph(0.1), &[], &[])
        .set_x_label("withers height (in)", &[])
        .set_y_label("body length (in)", &[])
        .points(x, y, &[Caption("Dog")]);
    fg.show();
    
    Ok(())
}
