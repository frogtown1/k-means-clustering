extern crate rusty_machine;
extern crate rand;
extern crate toml;

use rusty_machine::linalg::{Matrix, BaseMatrix};

use rand::thread_rng;
use rand::distributions::Distribution;
use rand_distr::Normal;
use serde::Deserialize;
use std::fs::read_to_string;
use std::io;
use std::vec::Vec;
use structopt::StructOpt;

#[derive(Deserialize)]
struct Config {
    centroids: [f64; 12],
    noise: f64,
    samples_per_centroid: usize,
}

#[derive(StructOpt)]
struct Options {
    #[structopt(short = "c", long = "config-file", parse(from_os_str))]
    config_file_path: std::path::PathBuf,
}
        
fn main() -> Result<(), std::io::Error> {
    let options = Options::from_args();
    let toml_config_str = read_to_string(options.config_file_path)?;
    let config: Config = toml::from_str(&toml_config_str)?;
    let centroids = Matrix::new(6, 2, config.centroids.to_vec());
    let samples = generate_training_data(&centroids, config.samples_per_centroid, config.noise);

    let mut writer = csv::Writer::from_writer(io::stdout());
    writer.write_record(&["withers height", "length"])?;

    for sample in samples.iter_rows() {
        writer.serialize(sample)?;
    }
    Ok(())
}

fn generate_training_data(centroids: &Matrix<f64>, points_per_centroid: usize, noise: f64) -> Matrix<f64> {
    assert!(centroids.cols() > 0, "Excuse me, but centroids cannot be empty.");
    assert!(centroids.rows() > 0, "Excuse me, but centroids cannot be empty.");
    assert!(noise >= 0f64, "Excuse me, but noise must be a non-negative value.");

    let mut raw_cluster_data =
        Vec::with_capacity(centroids.rows() * points_per_centroid * centroids.cols());
    let mut rng = thread_rng();
    let normal_rv = Normal::new(0f64, noise).unwrap();

    for _ in 0..points_per_centroid {
        for centroid in centroids.iter_rows() {
            // generate random point around the centroid
            let mut point = Vec::with_capacity(centroids.cols());

            for feature in centroid.iter() {
                point.push(feature + normal_rv.sample(&mut rng));
            }

            raw_cluster_data.extend(point);
        }
    }
    // convert to a matrix of samples, one sample per row
    Matrix::new(centroids.rows() * points_per_centroid, centroids.cols(), raw_cluster_data)
}
