use std::error::Error;
use gnuplot::{Figure, Caption, Graph, Color, PointSymbol};
use gnuplot::AxesCommon;
use std::io;

fn main() -> Result<(), Box<dyn Error>> {
    let mut x: [Vec<f64>; 6] = [Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new()];
    let mut y: [Vec<f64>; 6] = [Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new(), Vec::new()];
    let mut reader = csv::Reader::from_reader(io::stdin());

    for result in reader.records() {
        let record = result?;
        let class: usize = record[2].parse().unwrap();
        x[class].push(record[0].parse().unwrap());
        y[class].push(record[1].parse().unwrap());
    }
    
    let mut fg = Figure::new();
    fg.axes2d()
        .set_title("Dog Body Measurements", &[])
        .set_legend(Graph(0.9), Graph(0.1), &[], &[])
        .set_x_label("withers height (in)", &[])
        .set_y_label("body length (in)", &[])
        .points(&x[0], &y[0], &[Caption("Dalmatian"), Color("blue"), PointSymbol('+')],)
        .points(&x[1], &y[1], &[Caption("Irish Wolfhound"), Color("green"), PointSymbol('x')],)
        .points(&x[2], &y[2], &[Caption("Chihuahua"), Color("orange"), PointSymbol('o')],)
        .points(&x[3], &y[3], &[Caption("Alaskan Malamute"), Color("purple"), PointSymbol('x')],)
        .points(&x[4], &y[4], &[Caption("Pembroke Welsh Corgi"), Color("black"), PointSymbol('+')],)
        .points(&x[5], &y[5], &[Caption("Collie"), Color("red"), PointSymbol('o')],)
        ;
    fg.show();
    
    Ok(())
}
